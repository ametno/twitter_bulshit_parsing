#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

#
# config_parser.py
#
# Auteur : Noël
#
# Descriptif : Lit la configuration du fichier config.xml
#

from lxml import etree


# TODO : toutes les clés sont statiques, revoir cette fonction pour que ça soit mieux
def config_parser():
    """ Récupère la configuration du fichier config.xml. Retourne un dictionnaire ou false en cas d'erreur."""
    
    configuration = {}
    
    # Tente d'ouvrir le fichier de configuration
    try:
        tree = etree.parse("config.xml")
        key_in_config = ['host', 'user', 'base', 'port', 'password']

        # Pour chaque clé attendues
        for key in key_in_config:
            # Récupère les données
            for valeur_dans_ligne in tree.xpath("/config/bdd/"+key):
                # Si la donnée est nulle, elle est transformée en str
                if valeur_dans_ligne.text is None:
                    configuration[key] = ""
                else:
                    configuration[key] = valeur_dans_ligne.text

        for url in tree.xpath("/config/app/url_twitter"):
            configuration['url_twitter'] = url.text

    except IOError:
        return False
    return configuration
