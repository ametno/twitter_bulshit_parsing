#!/usr/bin/env python3
# -*- coding:Utf-8 -*-
## refas_amine
import urllib3
import parser
from config_parser import config_parser

class Controler(object):
    url = ""
    def __init__(self):
        configurateur = config_parser()
        self.url = configurateur['url_twitter']
        
    def process_array_to_string(self,search):
        search["keywords"] = search["keywords"].replace(' ','%20')
        stringSearch = search["username"] + search["keywords"]
        return stringSearch
            
    def send_twitter(self,search_result):
        # transform array to string
        stringSearch = self.process_array_to_string(search_result)
        # no display warnings about https security
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        http = urllib3.PoolManager()
        # send request to Twitter
        requete = http.request('get', self.url.format(stringSearch))
        return requete
        parserRequest = parser.Parser()
        # get page of Twitter
        parserRequest.parse_requete(requete.data)
        

