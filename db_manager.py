#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

#
# fichier : db_manager.py
# auteur : Noël
#
# description :
# s'occupe des connexions à la bdd et se charge des actions à réaliser dessus

from config_parser import config_parser
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Table, DateTime, Text

from tweet import Twitter


config = config_parser()

class Db_manager(object):
    # Déclare l'objet permettant la connexion

    def __init__(self):

        ########################
        #
        # Configuration de la base de données
        #
        ########################
        if config['password'] is "" and config["port"] is "":
            a_connecter = "mysql+pymysql://"+config['user']+"@"+config['host']+"/"+config["base"]

        elif config['password'] is "" and config["port"] is not "":
            a_connecter = "mysql+pymysql://" + config['user'] + "@" + config['host'] + ":"+config['port']+"/" +\
                          config["base"]

        elif config['password'] is not "" and config["port"] is "":
            a_connecter = "mysql+pymysql://" + config['user'] + ":"+ config['password'] + "@" + config['host'] + "/" + \
                          config["base"]

        elif config['password'] is not "" and config["port"] is not "":
            a_connecter = "mysql+pymysql://" + config['user'] + ":"+ config['password'] + "@" + config['host'] + ":" + \
                          config['port'] + "/" + config["base"]


        #########################
        #
        # Configuration de la base et de la session vers la bdd
        #
        #########################
        # Création de l'engine
        self.engine = create_engine(a_connecter)

        # Créé une session sql
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()
        self.Base = declarative_base()


        ##########################
        #
        # Création de la table twitter si elle n'existe pas
        #
        ##########################
        self.twitter_table = Table('twitter', self.Base.metadata,
            Column('id', Integer, primary_key=True),
            Column('idTweet', Integer),
            Column('imgUser', String(1000)),
            Column('dateTweet', DateTime),
            Column('bodyTweet',Text))

        ##########################
        #
        # Création de la table recherche si elle n'existe pas
        #
        ##########################
        self.recherche_table = Table('recherche', self.Base.metadata,
            Column('id', Integer, primary_key=True),
            Column('username', String(100)),
            Column('tag', String(100)),
            Column('duree', Integer))

        # Création des tables
        self.Base.metadata.create_all(self.engine)

    def select_tweets(self, id=0):
        """Sélectionne tous les tweets
        id (int) : sélectionne un tweet à partir de son id. Si id = 0, retourne tous les tweets"""

        if id:
            return self.session.query(self.twitter_table).filter_by(idTweet=id).all()

        else:
            return self.session.query(self.twitter_table).all()

    def insert_twitter(self, nouveau_tweet):
        idTweet = nouveau_tweet.idTweet
        imgUser = nouveau_tweet.imgUser
        date = nouveau_tweet.dateTweet
        body = nouveau_tweet.bodyTweet
        body = body.replace("'","\\'")
        #print("INSERT INTO twitter VALUES(0, "+str(idTweet)+",'"+imgUser+"','"+date+"','"+body+"')")
        self.engine.execute("INSERT INTO twitter VALUES(0, "+str(idTweet)+",'"+imgUser+"','"+date+"','"+body+"')")

#test_insertion_tweet = Twitter(idTweet=45,imgUser="profil.jpg",bodyTweet="Ceci est un test n°2")
#manager = Db_manager()
#manager.insert_twitter(test_insertion_tweet)