#!/usr/bin/env python3
# -*- coding:Utf-8 -*-
# refas_amine
#09:08 - 29 janv. 2018
from bs4 import BeautifulSoup
import tweet
from datetime import datetime

# strtime pour la date
class Parser(object):
    def parse_requete(self,html_page):
        tweets_list = []
        soup = BeautifulSoup(html_page, 'html.parser')        
        for tweets in soup.find_all("li", {"data-item-type": "tweet"}):
            id_tweet = tweets.div['data-item-id']
            img_user_tweet = tweets.div.a.img['src']
            date_tweet = self.convert_twitter_date(tweets.div.small.a['title'])
            body_tweet = tweets.div.p.getText()
            # call model Twitter
            twitter = tweet.Twitter(id_tweet,img_user_tweet,date_tweet,body_tweet) 
            tweets_list.append(twitter)
        return tweets_list
    
    def convert_twitter_date(self,twitter_date):
        datetime_obj = datetime.strptime(twitter_date, "%H:%M - %d %b %Y")
        return datetime_obj.strftime('%d/%m/%Y')      