#!/usr/bin/env python3
# -*- coding:Utf-8 -*-
# refas_amine
from tkinter import *
from controler import Controler
from parser import Parser


class Template(Frame):
    def __init__(self, fenetre, **kwargs):
        # On crée une fenêtre, racine de notre interface
        Frame.__init__(self, fenetre, width=1000, height=800, **kwargs)
        fenetre.geometry("2000x800")
        # Création de nos widgets  
        self.user_label = Label(fenetre, text="Username")
        self.user_label.grid(row=0, column=0, ipadx=50)
        # stock search input of user
        self.search = {}
        
        self.var_user = StringVar(fenetre)
        self.user_texte = Entry(fenetre, textvariable=self.var_user, width=20)
        self.user_texte.grid(row=0, column=1, ipadx=50,pady=5)
        
        self.keywords_label = Label(fenetre, text="Keywords")
        self.keywords_label.grid(row=1, column=0, ipadx=50)
        
        self.var_keywords = StringVar(fenetre)
        self.keywords_texte = Entry(fenetre, textvariable=self.var_keywords, width=20)
        self.keywords_texte.grid(row=1, column=1, ipadx=50,pady=5)

        # button
        self.bouton_quitter = Button(fenetre, text="Rechercher", command=self.get_search)
        self.bouton_quitter.grid(row=2, column=1, ipadx=50,pady=15)
        
        
        self.liste_tweet = Listbox(fenetre,width = 200)
        self.liste_tweet.grid(row=3, column=1, ipadx=50)        
        
        #add observer to check field empty
        #self.var_user.trace("w", self.check_empty_field)
        #self.var_keywords.trace("w", self.check_empty_field)
        
        
    def get_search(self):
        self.liste_tweet.delete(0, END)
        #get search user input 
        self.search["username"] = self.var_user.get()
        self.search["keywords"] = self.var_keywords.get()
        controller = Controler()
        requete = controller.send_twitter(self.search)
        #check if request is found
        if(requete.status == 200):
            parserRequest = Parser()
            tweet_list = parserRequest.parse_requete(requete.data)
            #check if list contain tweet
            if(tweet_list):
                for tweet in tweet_list:
                    self.liste_tweet.insert(ACTIVE,"ID : " + tweet.idTweet +"   |    "+ "Message : " + tweet.bodyTweet + "   |  " +"Date : " + tweet.dateTweet )
            else:
                self.liste_tweet.insert(END,"Aucun résultat pour cette recherche");   
        
    
    def check_empty_field(self):
        username = self.var_user.get()
        keywords = self.var_keywords.get()
        if username and keywords:
            self.bouton_quitter.config(state='normal')
        else:
            self.bouton_quitter.config(state='disabled')
        
