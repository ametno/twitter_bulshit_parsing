#!/usr/bin/env python3
# -*- coding:Utf-8 -*-
import datetime
class Twitter(object):
    idTweet = 0
    imgUser = ""
    dateTweet = ""
    bodyTweet = ""
    def __init__(self, idTweet, imgUser, dateTweet=datetime.datetime.now().strftime('%d/%m/%Y'), bodyTweet=""):
        self.idTweet = idTweet
        self.imgUser = imgUser
        self.dateTweet = dateTweet
        self.bodyTweet = bodyTweet
